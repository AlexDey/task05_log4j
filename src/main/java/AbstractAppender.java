import org.apache.logging.log4j.core.LogEvent;

public abstract class AbstractAppender {
    abstract void append(LogEvent event);
}
